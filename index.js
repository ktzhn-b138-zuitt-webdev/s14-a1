console.log("Hello World");

let firstName = "John";
let lastName = "Smith";
let age = 30;

console.log("First Name: " + firstName);
console.log("Last Name: " + lastName);
console.log("Age: " + age);

console.log("Hobbies: ");

let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
console.log(hobbies);

console.log("Work Address: ");

let loc = {
	city: "Lincoln",
	houseNumber: 32,
	state: "Nebraska",
	street: "Washington",
}

console.log(loc);

function usrDetails(firstName, lastName, age, loc, hobbies){
	console.log(firstName + " " + lastName + " is " + age + " years of age.");
	console.log("Hobbies: ");
	console.log(hobbies);
	console.log("Work Address: ");
	console.log(loc);
}

// Calling the function
usrDetails(firstName, lastName, age, loc, hobbies);


// Returning the function and storing as variable
let stuff;

function returnUsrDetails(hobbies) {
	return hobbies;
}

console.log("This is a returned value stored in a variable:")
stuff = returnUsrDetails(hobbies)

console.log(stuff);